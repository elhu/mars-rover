require 'spec_helper'

describe Rover do
  let(:plateau) { Plateau.new(max_x: 4, max_y: 5) }
  let(:err_stream) { StringIO.new }

  describe '#initialize' do
    context 'with valid coordinates and orientation' do
      subject { described_class.new(plateau: plateau, x: 3, y: 4, orientation: 'N') }

      it 'sets x, y, and orientation' do
        expect(subject.x).to eq(3)
        expect(subject.y).to eq(4)
        expect(subject.orientation).to eq('N')
      end

      it 'accepts 0 for x and y' do
        expect {
          rover = described_class.new(plateau: plateau, x: 0, y: 0, orientation: 'N')
          expect(rover.x).to eq(0)
          expect(rover.y).to eq(0)
          expect(rover.orientation).to eq('N')
        }.not_to raise_error
      end
    end

    context 'with invalid coordinates' do


      it 'it corrects the coordinates with a negative x' do
        rover = described_class.new(plateau: plateau, x: -1, y: 1, orientation: 'N', error_output: err_stream)
        expect(rover.x).to eq(0)
        expect(rover.y).to eq(0)

        err_stream.rewind
        expect(err_stream.read).to match(/\[RoverDeployment\] Invalid coordinates/)
      end

      it 'it corrects the coordinates with a negative y' do
        rover = described_class.new(plateau: plateau, x: 1, y: -1, orientation: 'N', error_output: err_stream)
        expect(rover.x).to eq(0)
        expect(rover.y).to eq(0)

        err_stream.rewind
        expect(err_stream.read).to match(/\[RoverDeployment\] Invalid coordinates/)
      end

      it 'it corrects the coordinates with an x greater than the plateau\'s max_x' do
        rover = described_class.new(plateau: plateau, x: 5, y: 1, orientation: 'N', error_output: err_stream)
        expect(rover.x).to eq(0)
        expect(rover.y).to eq(0)

        err_stream.rewind
        expect(err_stream.read).to match(/\[RoverDeployment\] Invalid coordinates/)
      end

      it 'it corrects the coordinates with an y greater than the plateau\'s max_y' do
        rover = described_class.new(plateau: plateau, x: 1, y: 6, orientation: 'N', error_output: err_stream)
        expect(rover.x).to eq(0)
        expect(rover.y).to eq(0)

        err_stream.rewind
        expect(err_stream.read).to match(/\[RoverDeployment\] Invalid coordinates/)
      end
    end

    context 'with invalid orientation' do
      it 'does not accept an orientation other than N E S W' do
        rover = described_class.new(plateau: plateau, x: 3, y: 4, orientation: 'Z', error_output: err_stream)
        expect(rover.orientation).to eq('N')

        err_stream.rewind
        expect(err_stream.read).to match(/\[RoverDeployment\] Invalid orientation/)
      end
    end
  end

  describe '#send_command' do
    subject { described_class.new(plateau: plateau, x: 3, y: 4, orientation: 'N', error_output: err_stream) }

    it 'calls #move if given M' do
      expect(subject).to receive(:move)
      subject.send_command('M')
    end

    it 'calls #rotate with left if given L' do
      expect(subject).to receive(:rotate).with(direction: :left)
      subject.send_command('L')
    end

    it 'calls #rotate with right if given R' do
      expect(subject).to receive(:rotate).with(direction: :right)
      subject.send_command('R')
    end

    it 'warns the operator if the command is not known' do
      subject.send_command('Z')

      err_stream.rewind
      expect(err_stream.read).to match(/\[RoverDeployment\] Ignoring unknown command/)
    end

    describe 'movement' do
      let(:x) { 2 }
      let(:y) { 2 }

      context 'when facing north' do
        subject { described_class.new(plateau: plateau, x: x, y: y, orientation: 'N') }

        it 'increments y by 1' do
          expect {
            expect {
              subject.send_command('M')
            }.to change(subject, :y).by(1)
          }.not_to change(subject, :x)
        end
      end

      context 'when facing east' do
        subject { described_class.new(plateau: plateau, x: x, y: y, orientation: 'E') }

        it 'increments x by 1' do
          expect {
            expect {
              subject.send_command('M')
            }.to change(subject, :x).by(1)
          }.not_to change(subject, :y)
        end
      end

      context 'when facing south' do
        subject { described_class.new(plateau: plateau, x: x, y: y, orientation: 'S') }

        it 'decrements y by 1' do
          expect {
            expect {
              subject.send_command('M')
            }.to change(subject, :y).by(-1)
          }.not_to change(subject, :x)
        end
      end

      context 'when facing west' do
        subject { described_class.new(plateau: plateau, x: x, y: y, orientation: 'W') }

        it 'decrements x by 1' do
          expect {
            expect {
              subject.send_command('M')
            }.to change(subject, :x).by(-1)
          }.not_to change(subject, :y)
        end
      end
    end

    describe 'rotation' do
      let(:orientation) { 'N' }
      subject { described_class.new(plateau: plateau, x: 0, y: 0, orientation: orientation) }

      context 'rotating left' do
        it 'goes back to the original when called 4 times' do
          expect {
            4.times { subject.send_command('L') }
          }.not_to change(subject, :orientation)
        end

        context 'from NORTH' do
          let(:orientation) { 'N' }
          it 'changes the orientation to WEST' do
            expect {
              subject.send_command('L')
            }.to change(subject, :orientation).to('W')
          end
        end

        context 'from WEST' do
          let(:orientation) { 'W' }
          it 'changes the orientation to SOUTH' do
            expect {
              subject.send_command('L')
            }.to change(subject, :orientation).to('S')
          end
        end

        context 'from SOUTH' do
          let(:orientation) { 'S' }
          it 'changes the orientation to EAST' do
            expect {
              subject.send_command('L')
            }.to change(subject, :orientation).to('E')
          end
        end

        context 'from EAST' do
          let(:orientation) { 'E' }
          it 'changes the orientation to NORTH' do
            expect {
              subject.send_command('L')
            }.to change(subject, :orientation).to('N')
          end
        end
      end

      context 'rotating right' do
        it 'goes back to the original when called 4 times' do
          expect {
            4.times { subject.send_command('R') }
          }.not_to change(subject, :orientation)
        end

        context 'from NORTH' do
          let(:orientation) { 'N' }
          it 'changes the orientation to EAST' do
            expect {
              subject.send_command('R')
            }.to change(subject, :orientation).to('E')
          end
        end

        context 'from EAST' do
          let(:orientation) { 'E' }
          it 'changes the orientation to SOUTH' do
            expect {
              subject.send_command('R')
            }.to change(subject, :orientation).to('S')
          end
        end

        context 'from SOUTH' do
          let(:orientation) { 'S' }
          it 'changes the orientation to WEST' do
            expect {
              subject.send_command('R')
            }.to change(subject, :orientation).to('W')
          end
        end

        context 'from WEST' do
          let(:orientation) { 'W' }
          it 'changes the orientation to NORTH' do
            expect {
              subject.send_command('R')
            }.to change(subject, :orientation).to('N')
          end
        end
      end
    end
  end
end
