require 'spec_helper'

describe ControlPanel do
  let(:max_x) { 5 }
  let(:max_y) { 5 }

  describe '#initialize' do
    it 'creates a new plateau with the given coordinates' do
      expect(Plateau).to receive(:new).with(max_x: max_x, max_y: max_y)
      described_class.new(max_x: max_x, max_y: max_y)
    end
  end

  describe '#start!' do
    it 'returns immediately if the plateau is invalid' do
      control_panel = described_class.new(max_x: max_x, max_y: -1)
      expect(control_panel).not_to receive(:handle_rover)
      expect(control_panel.start!).to eq(false)
    end

    it 'calls handle_rover as many times as we have rovers in the input (+ 1)' do
      input = StringIO.new("1 2 N\nLMLMLMLMM\n3 3 E\nMMRMMRMRRM\n")
      output = StringIO.new
      control_panel = described_class.new(max_x: max_x, max_y: max_y, input: input, output: output)
      expect(control_panel).to receive(:handle_rover).thrice.and_call_original
      control_panel.start!

      output.rewind
      expect(output.read).to eq("1 3 N\n5 1 E\n")
    end
  end
end
