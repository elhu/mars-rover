require 'spec_helper'

describe Plateau do
  subject { described_class.new(max_x: 4, max_y: 5) }

  describe '#initialize' do
    it 'sets the coordinates' do
      expect(subject.max_x).to eq(4)
      expect(subject.max_y).to eq(5)
    end
  end

  describe '#valid?' do
    context 'with valid coordinates' do
      it 'returns true' do
        expect(subject.valid?).to be true
      end
    end

    context 'with invalid coordinates' do
      it 'does not accept a negative max_x' do
        expect(described_class.new(max_x: -4, max_y: 5).valid?).to be false
      end

      it 'does not accept a negative max_y' do
        expect(described_class.new(max_x: 4, max_y: -5).valid?).to be false
      end
    end
  end

  describe '#valid_coordinates?' do
    it 'returns true if x and y are within bounds' do
      expect(subject.valid_coordinates?(4, 5)).to be true
    end

    it 'returns false if x < 0' do
      expect(subject.valid_coordinates?(-1, 5)).to be false
    end

    it 'returns false if x > max_x' do
      expect(subject.valid_coordinates?(5, 5)).to be false
    end

    it 'returns false if y < 0' do
      expect(subject.valid_coordinates?(4, -1)).to be false
    end

    it 'returns false if y > max_y' do
      expect(subject.valid_coordinates?(4, 6)).to be false
    end
  end

end
