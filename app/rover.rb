require 'plateau'

class Rover
  attr_reader :x, :y, :orientation

  ORIENTATIONS = %w(N E S W).freeze

  def initialize(plateau:, x:, y:, orientation:, error_output: STDERR)
    @plateau = plateau
    @x = x.to_i
    @y = y.to_i
    @orientation = orientation.to_s
    @error_output = error_output
    rover_deployment_correction!
  end

  def report_coordinates
    "#{x} #{y} #{orientation}"
  end

  def send_command(command)
    command = command.to_s
    case command
    when 'M'
      move
    when 'L'
      rotate(direction: :left)
    when 'R'
      rotate(direction: :right)
    else
      @error_output.puts "[RoverDeployment] Ignoring unknown command #{command}"
    end
  end

  private
  def move
    original_x, original_y = x, y

    case orientation
    when 'N'
      @y += 1
    when 'E'
      @x += 1
    when 'S'
      @y -= 1
    when 'W'
      @x -= 1
    end
    if !@plateau.valid_coordinates?(x, y)
      @error_output.puts "[RoverDeployment] Preventing move to invalid coordinates (#{x}, #{y})"
      @x, @y = original_x, original_y
    end
  end

  def rotate(direction:)
    direction = direction.to_sym
    case direction
    when :left
      rotate_left
    when :right
      rotate_right
    end
  end

  def rotate_left
    orientation_index = ORIENTATIONS.index(orientation)
    base_index = orientation_index == 0 ? ORIENTATIONS.length : orientation_index
    @orientation = ORIENTATIONS[base_index - 1]
  end

  def rotate_right
    orientation_index = ORIENTATIONS.index(orientation)
    base_index = orientation_index == ORIENTATIONS.length - 1 ? -1 : orientation_index
    @orientation = ORIENTATIONS[base_index + 1]
  end

  def rover_deployment_correction!
    if !@plateau.valid_coordinates?(x, y)
      @error_output.puts "[RoverDeployment] Invalid coordinates (#{x}, #{y}), correcting to 0, 0"
      @x = 0
      @y = 0
    end
    if !ORIENTATIONS.include?(orientation)
      @error_output.puts "[RoverDeployment] Invalid orientation (#{orientation}), correcting to N"
      @orientation = 'N'
    end
  end
end
