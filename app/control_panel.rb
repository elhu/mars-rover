require 'plateau'
require 'rover'

class ControlPanel
  def initialize(max_x:, max_y:, input: STDIN, output: STDOUT)
    @plateau = Plateau.new(max_x: max_x, max_y: max_y)
    @input = input
    @output = output
  end

  def start!
    return false unless @plateau.valid?
    loop do
      break unless handle_rover
    end
  end

  private
  def handle_rover
    if (rover = init_rover)
      if handle_commands(rover)
        @output.puts rover.report_coordinates
        return true
      end
    end
    STDERR.puts "No more rover or commands, aborting"
    return false
  end

  def init_rover
    params = @input.gets
    return false if params.nil?
    x, y, orientation = params.strip.split(' ')
    Rover.new(plateau: @plateau, x: x, y: y, orientation: orientation)
  end

  def handle_commands(rover)
    commands = @input.gets
    return false if commands.nil?
    commands.strip.split('').each do |command|
      rover.send_command(command)
    end
  end
end
