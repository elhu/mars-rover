class Plateau
  attr_reader :max_x, :max_y

  def initialize(max_x:, max_y:)
    @max_x = max_x.to_i
    @max_y = max_y.to_i
  end

  def valid_coordinates?(x, y)
    (0..(max_x)).include?(x) && (0..(max_y)).include?(y)
  end

  def valid?
    max_x >= 0 && max_y >= 0
  end
end
