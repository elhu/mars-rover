#! /usr/bin/env ruby

$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), 'app'))

require 'control_panel'

# Getting plateau's dimensions
plateau_dimensions = gets
if plateau_dimensions.nil?
  STDERR.puts "Unable to get plateau dimensions, aborting"
  exit(1)
end
max_x, max_y = plateau_dimensions.split(' ').map(&:to_i)
panel = ControlPanel.new(max_x: max_x, max_y: max_y)

# Processing rovers
panel.start!
