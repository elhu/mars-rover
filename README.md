# Mars Rover expedition

This program is control panel for the Mars Rover expedition.

## Running the program

Checkout the project, and install dependencies by running:

```
$> bundle install
```

You can then run the program by typing:

```
$> ./rovers.rb
```

## Input

The first line of input is the upper-right coordinates of the plateau, the lower-left coordinates are assumed to be 0,0.

The rest of the input is information pertaining to the rovers that have been deployed. Each rover has two lines of input. The first line gives the rover's position, and the second line is a series of instructions telling the rover how to explore the plateau.

The position is made up of two integers and a letter separated by spaces, corresponding to the x and y co-ordinates and the rover's orientation.

Each rover will be finished sequentially, which means that the second rover won't start to move until the first one has finished moving.

## Output

The output for each rover should be its final co-ordinates and heading.

## Example

*Test Input:*

```
5 5
1 2 N
LMLMLMLMM
3 3 E
MMRMMRMRRM
```

*Expected Output:*

```
1 3 N
5 1 E
```

## Caveats

### Rovers deployed outside of the plateau

To avoid human error, the Mars Rover expedition rover deployment software automatically corrects the deployment location to the South-West of the plateau (0, 0) in case of invalid deployment coordinates.

Invalid deployment coordinates will result in a warning message printed for the rover operator.

### Invalid orders

Rovers have been programmed to ignore invalid orders.
The following orders are considered invalid:
* `M`oving outside the bounds of the plateau.
* Any command other than `M`, `L` and `R`.

Invalid orders will result in a warning message printed for the rover operator.

### Rover collision

Rovers are outfitted with the latest developments in collision prevention technology.
Furthermore, they are sufficiently small to be able to be at the same time at the same coordinates.

Therefore, there can be no collisions between rovers.

## Contributing

Change anything you'd like, make sure the specs still pass, and document it!

